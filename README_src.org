#+OPTIONS: toc:nil author:nil title:nil
#+EXPORT_FILE_NAME: README.md

* Debase, the D-Bus convenience layer for Emacs
  :PROPERTIES:
  :ID:       2055fdfe-f336-4c8c-b238-978bfe84d09c
  :END:

  #+TITLE: Debase, illustrated
  [[file:sorry.jpg]]

  D-Bus is an [[https://en.wikipedia.org/wiki/Inter-process_communication][IPC system]] which is ubiquitous on Linux, and (in this
  author’s opinion) not very good.  Emacs has bindings for interfacing
  with it (see the former point), which are annoying to use (see the
  latter point).

  These days, numerous common system management tasks are implemented
  as D-Bus services rather than tradidional executables, and many of
  the command-line tools themselves are now front-ends which
  communicate via D-Bus.  Mounting and unmounting disks, monitoring
  battery status, controlling display brightness, connecting to
  wireless networks and more are now handled with D-Bus services.

  It makes no sense to shell out to the tools when one could interact
  with them directly via D-Bus, if only it was less annoying to do so.

  Debase frees you from writing repetitive, annoying boilerplate code
  to drive D-Bus services by throwing another pile of abstraction at
  the problem, in the form of unreadably dense, macro-heavy,
  profoundly cursed Lisp.


** D-Bus Crash Course
   :PROPERTIES:
   :ID:       d00772f4-8bfe-4f63-8d7f-ccc43d2586f2
   :END:

   - Bus.  A bus contains services; D-Bus can manage many different
     busses, but the two standard ones are:
     - System bus.  This generally has hardware-interfacing and system
       management services.
     - Session bus.  This is private to the current session, i.e. a
       logged-in user.

   - Services.  A service exists on a bus, and is a set of information
     and operations offered by a program.  Example: =org.bluez= on the
     system bus is the service which manages Bluetooth.

   - Objects.  An object exists within a service, and typically
     represents a resource it manages.  Objects are identified by
     paths; paths are namespaced under the service.  Example:
     =/org/bluez/hci0/dev_01_23_45_67_89_AB= is the path to an object
     representing a specific Bluetooth device.  Because this is part
     of the service, that path doesn’t represent anything in a
     different service, like =org.freedesktop.fwupd=.

   - Interfaces.  An interface is a view into the capabilities of an
     object.  Objects can (and almost always do) support multiple
     interfaces.  Example: =org.bluez.Device1= is a general interface
     for managing pairing/unpairing/connecting/disconnecting from
     Bluetooth devices; =org.bluez.MediaControl1= is an interface for
     media devices, such as speakers or speakerphones.  Since
     =/org/bluez/hci0/dev_01_23_45_67_89_AB= is a media device, it
     supports both interfaces.

   - Properties.  A property is a value attached to an interface,
     which exposes information about an object.  For example, the
     =Name= property in the =org.bluez.Device1= interface of
     =/org/bluez/hci0/dev_01_23_45_67_89_AB= is "Bluetooth Speaker" —
     the name of the device.  Properties can be read/write, read-only,
     or write-only.

   - Methods.  A method is a remote function call attached to an
     interface.  For example, the =VolumeUp()= method in the
     =org.bluez.MediaControl1= interface of object
     =/org/bluez/hci0/dev_01_23_45_67_89_AB= in the =org.bluez=
     service of the system bus increases the volume of "Bluetooth
     Speaker."  Methods can take arguments and return values.

   - Signals.  D-Bus enabled applications can generate and respond to
     signals.  A signal represents some kind of event, such as
     hardware being plugged in or unplugged.

   - Common interfaces.  /Most/ D-Bus objects support some common
     interfaces:

     - [[https://dbus.freedesktop.org/doc/dbus-java/api/org/freedesktop/DBus.Introspectable.html][=org.freedesktop.DBus.Introspectable=]].  Allows retrieving the
       schema for the object as XML.  It has all the interfaces it
       supports, as well as their properties and methods.
     - [[https://dbus.freedesktop.org/doc/dbus-java/api/org/freedesktop/DBus.Peer.html][=org.freedesktop.DBus.Peer=]].  Provides a =Ping= method.
     - [[https://dbus.freedesktop.org/doc/dbus-java/api/org/freedesktop/DBus.Properties.html][=org.freedesktop.DBus.Properties=]].  An interface which exposes
       object properties, and provides signals so other D-Bus
       applications receive notifications of changes to them.
     - [[https://dbus.freedesktop.org/doc/dbus-specification.html#standard-interfaces-objectmanager][=org.freedesktop.DBus.ObjectManager=]].  Used by D-Bus
       applications which manage other D-Bus objects.  For example,
       the =org.bluez= service’s =/= object implements
       =ObjectManager=, which can be used to enumerate connected
       Bluetooth devices.  It also provides signals when managed
       objects are added or removed.


** Debase Objects
   :PROPERTIES:
   :ID:       a7ea0d8e-9923-4cd8-9d15-0e69158fda85
   :END:

   Debase defines a =DEBASE-OBJECT= EIEIO base class, which acts as a
   proxy between Emacs Lisp and the D-Bus service.  A =DEBASE-OBJECT=
   maps 1:1 with a D-Bus object, and stores the bus, service, path,
   and (optionally) interface of that object.

   #+BEGIN_SRC emacs-lisp :eval never
     (setf upower (debase-object :bus :system
                                 :service "org.freedesktop.UPower"
                                 :path "/org/freedesktop/UPower"
                                 :interface "org.freedesktop.UPower"))
   #+END_SRC

   Since many D-Bus objects use identical (or readily computable)
   values for service, path, and interface, you may omit the path
   and/or interface, and Debase will fill them in with what seem like
   reasonable values.  The =DEBASE-TARGET= function will return a
   plist of these values, whether computed or provided explicitly.

   #+BEGIN_SRC emacs-lisp :results value verbatim :exports both
     (debase-object-target
      (debase-object :bus :system :service "org.freedesktop.UPower"))
   #+END_SRC

   #+RESULTS:
   : (:bus :system :service "org.freedesktop.UPower" :path "/org/freedesktop/UPower" :interface "org.freedesktop.UPower")

   #+BEGIN_SRC emacs-lisp :results value verbatim
     (debase-object-target
      (debase-object :bus :system
                     :service "org.freedesktop.UDisks2"
                     :interface "org.freedesktop.UDisks2.Manager"))
   #+END_SRC

   #+RESULTS:
   : (:bus :system :service "org.freedesktop.UDisks2" :path "/org/freedesktop/UDisks2/Manager" :interface "org.freedesktop.UDisks2.Manager")

   Debase also provides generic functions which mirror the ones in
   =dbus.el=, but take a single =DEBASE-OBJECT= instance instead of
   the bus/service/path/interface.  So instead of slogging through a
   dozen lengthy variations:

   #+BEGIN_SRC emacs-lisp :results value verbatim
     (list
      (dbus-get-property :system "org.freedesktop.UDisks2" "/org/freedesktop/UDisks2/Manager" "org.freedesktop.UDisks2.Manager" "Version")
      (dbus-get-property :system "org.freedesktop.UDisks2" "/org/freedesktop/UDisks2/Manager" "org.freedesktop.UDisks2.Manager" "SupportedFilesystems"))
   #+END_SRC

   #+RESULTS:
   : ("2.8.1" ("ext2" "ext3" "ext4" "vfat" "ntfs" "exfat" "xfs" "reiserfs" "nilfs2" "btrfs" "minix" "udf" "f2fs" "swap"))

   You can set a single object and use it over and over:

   #+BEGIN_SRC emacs-lisp :results value verbatim
     (let ((udisks2-manager (debase-object :bus :system
                                           :service "org.freedesktop.UDisks2"
                                           :interface "org.freedesktop.UDisks2.Manager")))
       (list (debase-get-property udisks2-manager "Version")
             (debase-get-property udisks2-manager "SupportedFilesystems")))

   #+END_SRC

   #+RESULTS:
   : ("2.8.1" ("ext2" "ext3" "ext4" "vfat" "ntfs" "exfat" "xfs" "reiserfs" "nilfs2" "btrfs" "minix" "udf" "f2fs" "swap"))

   Replacing the =DBUS-= prefix of most =dbus.el= function names with
   =DEBASE-= should work, for example =DEBASE-GET-PROPERTY= instead of
   =DBUS-GET-PROPERTY=.


*** Retargeting
    :PROPERTIES:
    :ID:       ff136236-e61e-4b73-85e9-1ffdb42e547b
    :END:

    Many times, you’ll need to change the interface or path of a
    =DEBASE-OBJECT=, either to access a different facet of the object,
    or to access another object within the same service.  EIEIO’s
    =CLONE= function makes it easy to swap out any part of the
    targeted object:

    #+BEGIN_SRC emacs-lisp :results value verbatim
      (let* ((block (debase-object :bus :system
                                   :service "org.freedesktop.UDisks2"
                                   :path "/org/freedesktop/UDisks2/block_devices/sda1"
                                   :interface "org.freedesktop.UDisks2.Block"))
             (block-dev (substring (apply #'string (debase-object-get block "Device")) 0 -1))
             (partition (clone block :interface "org.freedesktop.UDisks2.Partition")))
        (list block-dev (debase-object-get partition "UUID")))

    #+END_SRC

    #+RESULTS:
    : ("/dev/sda1" "43754339-01")


** Building Blocks
   :PROPERTIES:
   :ID:       6f38ce0d-f488-4ceb-aab2-771e83b2428d
   :END:

   Even though Debase makes this easier, many D-Bus methods require
   additional type wrangling or conversion to be used comfortably.
   For these cases, you should subclass =DEBASE-OBJECT= and write more
   specialized methods.

   #+BEGIN_SRC emacs-lisp :results value verbatim
     (defclass udisks2-block (debase-object) ())

     (cl-defmethod initialize-instance :after ((this udisks2-block) &rest ignore)
       (with-slots (bus service interface) this
         (setf bus :system
               service "org.freedesktop.UDisks2"
               interface  "org.freedesktop.UDisks2.Block")))

     (cl-defmethod udisks2-block-preferred-device ((this udisks2-block))
       "Returns the preferred device for `UDISKS2-BLOCK' object THIS."
       (substring (apply #'string (debase-object-get this "Device")) 0 -1))

     (let ((block (udisks2-block :path "/org/freedesktop/UDisks2/block_devices/sda1")))
       (udisks2-block-preferred-device block))
   #+END_SRC

   #+RESULTS:
   : "/dev/sda1"


*** Object Manager
    :PROPERTIES:
    :ID:       a55f476b-ed6a-4498-a4be-964cea6d2f29
    :END:

    Debase provides a =DEBASE-OBJECTMANAGER= class which interacts
    with the =org.freedesktop.DBus.ObjectManager= interface.  It
    maintains a local cache of managed objects, which is populated on
    instantiation and automatically updated when one is added or
    removed.

    If a class inherits from it, accessing the =MANAGED-OBJECTS= slot
    will return the currently managed objects.

    It can also dispatch notifications when the list of managed
    objects changes.


** Limitations
   :PROPERTIES:
   :ID:       1ad004df-82aa-4249-af27-8473d7b9ac75
   :END:

   - Support for providing D-Bus services from Emacs (which non-Emacs
     programs could invoke) is not supported.


** Code Generation (Experimental)
   :PROPERTIES:
   :ID:       3a84fa53-e074-457e-8ac7-bb75b7ab7703
   :END:

   Debase also offers a code generation facility, which turns the XML
   D-Bus interface descriptions into EIEIO classes.  The intent is to
   eliminate the drudgery of building the code that interacts with
   D-Bus, so you can focus on making it do interesting things instead.

   This is an experimental feature, and while I think it might be a
   good idea, I’ve struggled with usability for actual projects.
   Feedback and/or code welcomed.

   Codegen is implemented as a hierarchy of EIEIO classes which extend
   the =DEBASE-GEN= base class, and provide a =DEBASE-GEN-CODE=
   generic functions which produce the desired output.  The
   =DEBASE-GEN-CLASS= class is the main entrypoint.

   Basic example:

   #+BEGIN_SRC emacs-lisp :results code
     (thread-first
         (debase-gen-class :bus :system
                           :service "org.freedesktop.UDisks2"
                           :interface "org.freedesktop.UDisks2.Manager"
                           :class-name 'udisks2-manager)
       debase-gen-code)
   #+END_SRC

   #+RESULTS:
   #+begin_src emacs-lisp
   (prog1
       (defclass udisks2-manager
         (debase-object)
         ((version :type string :accessor version)
          (supported-filesystems :type t :accessor supported-filesystems))
         :documentation "Debase interface class for D-Bus interface \"org.freedesktop.UDisks2.Manager\"")
     (cl-defmethod version
       ((this udisks2-manager))
       (with-slots
           (bus service path interface)
           this
         (dbus-get-property bus service path interface "Version")))
     (cl-defmethod supported-filesystems
       ((this udisks2-manager))
       (with-slots
           (bus service path interface)
           this
         (dbus-get-property bus service path interface "SupportedFilesystems")))
     (cl-defmethod can-format
       ((obj udisks2-manager)
        type)
       "Return the results of calling D-Bus interface \"org.freedesktop.UDisks2.Manager\" method \"can-format\" on a `DEBASE-OBJECT' OBJ."
       (dbus-call-method this "can-format" type))
     (cl-defmethod can-resize
       ((obj udisks2-manager)
        type)
       "Return the results of calling D-Bus interface \"org.freedesktop.UDisks2.Manager\" method \"can-resize\" on a `DEBASE-OBJECT' OBJ."
       (dbus-call-method this "can-resize" type))
     (cl-defmethod can-check
       ((obj udisks2-manager)
        type)
       "Return the results of calling D-Bus interface \"org.freedesktop.UDisks2.Manager\" method \"can-check\" on a `DEBASE-OBJECT' OBJ."
       (dbus-call-method this "can-check" type))
     (cl-defmethod can-repair
       ((obj udisks2-manager)
        type)
       "Return the results of calling D-Bus interface \"org.freedesktop.UDisks2.Manager\" method \"can-repair\" on a `DEBASE-OBJECT' OBJ."
       (dbus-call-method this "can-repair" type))
     (cl-defmethod loop-setup
       ((obj udisks2-manager)
        fd options)
       "Return the results of calling D-Bus interface \"org.freedesktop.UDisks2.Manager\" method \"loop-setup\" on a `DEBASE-OBJECT' OBJ."
       (dbus-call-method this "loop-setup" fd options))
     (cl-defmethod mdraid-create
       ((obj udisks2-manager)
        blocks level name chunk options)
       "Return the results of calling D-Bus interface \"org.freedesktop.UDisks2.Manager\" method \"mdraid-create\" on a `DEBASE-OBJECT' OBJ."
       (dbus-call-method this "mdraid-create" blocks level name chunk options))
     (cl-defmethod enable-modules
       ((obj udisks2-manager)
        enable)
       "Return the results of calling D-Bus interface \"org.freedesktop.UDisks2.Manager\" method \"enable-modules\" on a `DEBASE-OBJECT' OBJ."
       (dbus-call-method this "enable-modules" enable))
     (cl-defmethod get-block-devices
       ((obj udisks2-manager)
        options)
       "Return the results of calling D-Bus interface \"org.freedesktop.UDisks2.Manager\" method \"get-block-devices\" on a `DEBASE-OBJECT' OBJ."
       (dbus-call-method this "get-block-devices" options))
     (cl-defmethod resolve-device
       ((obj udisks2-manager)
        devspec options)
       "Return the results of calling D-Bus interface \"org.freedesktop.UDisks2.Manager\" method \"resolve-device\" on a `DEBASE-OBJECT' OBJ."
       (dbus-call-method this "resolve-device" devspec options)))
   #+end_src


*** Name Mangling
    :PROPERTIES:
    :ID:       2975089a-7802-45fd-ac2f-f63b65c2d9cc
    :END:

    To make generated code more pleasant, =DEBASE-GEN= mangles D-Bus
    names into ones that are Lispier.  The default mangling is handled
    by =DEBASE-GEN-MANGLE=, but you can supply your own functions for
    properties, methods, and argument names.

    For example, to leave method and argument names untouched, and
    prefix properties with "Prop":

    #+BEGIN_SRC emacs-lisp :results code
      (thread-first
          (debase-gen-class :bus :system
                            :service "org.freedesktop.UDisks2"
                            :interface "org.freedesktop.UDisks2.Manager"
                            :class-name 'udisks2-manager
                            :property-mangle (debase-gen-mangle-prefix "Prop")
                            :method-mangle #'identity)
        debase-gen-code)
    #+END_SRC

    #+RESULTS:
    #+begin_src emacs-lisp
    (prog1
        (defclass udisks2-manager
          (debase-object)
          ((PropVersion :type string :accessor PropVersion)
           (PropSupportedFilesystems :type t :accessor PropSupportedFilesystems))
          :documentation "Debase interface class for D-Bus interface \"org.freedesktop.UDisks2.Manager\"")
      (cl-defmethod PropVersion
        ((this udisks2-manager))
        (with-slots
            (bus service path interface)
            this
          (dbus-get-property bus service path interface "Version")))
      (cl-defmethod PropSupportedFilesystems
        ((this udisks2-manager))
        (with-slots
            (bus service path interface)
            this
          (dbus-get-property bus service path interface "SupportedFilesystems")))
      (cl-defmethod CanFormat
        ((obj udisks2-manager)
         type)
        "Return the results of calling D-Bus interface \"org.freedesktop.UDisks2.Manager\" method \"CanFormat\" on a `DEBASE-OBJECT' OBJ."
        (dbus-call-method this "CanFormat" type))
      (cl-defmethod CanResize
        ((obj udisks2-manager)
         type)
        "Return the results of calling D-Bus interface \"org.freedesktop.UDisks2.Manager\" method \"CanResize\" on a `DEBASE-OBJECT' OBJ."
        (dbus-call-method this "CanResize" type))
      (cl-defmethod CanCheck
        ((obj udisks2-manager)
         type)
        "Return the results of calling D-Bus interface \"org.freedesktop.UDisks2.Manager\" method \"CanCheck\" on a `DEBASE-OBJECT' OBJ."
        (dbus-call-method this "CanCheck" type))
      (cl-defmethod CanRepair
        ((obj udisks2-manager)
         type)
        "Return the results of calling D-Bus interface \"org.freedesktop.UDisks2.Manager\" method \"CanRepair\" on a `DEBASE-OBJECT' OBJ."
        (dbus-call-method this "CanRepair" type))
      (cl-defmethod LoopSetup
        ((obj udisks2-manager)
         fd options)
        "Return the results of calling D-Bus interface \"org.freedesktop.UDisks2.Manager\" method \"LoopSetup\" on a `DEBASE-OBJECT' OBJ."
        (dbus-call-method this "LoopSetup" fd options))
      (cl-defmethod MDRaidCreate
        ((obj udisks2-manager)
         blocks level name chunk options)
        "Return the results of calling D-Bus interface \"org.freedesktop.UDisks2.Manager\" method \"MDRaidCreate\" on a `DEBASE-OBJECT' OBJ."
        (dbus-call-method this "MDRaidCreate" blocks level name chunk options))
      (cl-defmethod EnableModules
        ((obj udisks2-manager)
         enable)
        "Return the results of calling D-Bus interface \"org.freedesktop.UDisks2.Manager\" method \"EnableModules\" on a `DEBASE-OBJECT' OBJ."
        (dbus-call-method this "EnableModules" enable))
      (cl-defmethod GetBlockDevices
        ((obj udisks2-manager)
         options)
        "Return the results of calling D-Bus interface \"org.freedesktop.UDisks2.Manager\" method \"GetBlockDevices\" on a `DEBASE-OBJECT' OBJ."
        (dbus-call-method this "GetBlockDevices" options))
      (cl-defmethod ResolveDevice
        ((obj udisks2-manager)
         devspec options)
        "Return the results of calling D-Bus interface \"org.freedesktop.UDisks2.Manager\" method \"ResolveDevice\" on a `DEBASE-OBJECT' OBJ."
        (dbus-call-method this "ResolveDevice" devspec options)))
    #+end_src

*** Multiple Inheritance
    :PROPERTIES:
    :ID:       f56b2057-98b7-4d0c-857d-47550896dd22
    :END:

    Fully representing a D-Bus object with EIEIO classes means
    generating one class for each interface it has, then creating a
    new class which inherits from all of them.

    I haven’t found a nice way of making this easy yet, so you’re on
    your own.
